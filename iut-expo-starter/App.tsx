import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { useSafeAreaInsets, SafeAreaProvider } from 'react-native-safe-area-context';
import TabNavigation from './navigation/TabNavigation';
import CityList from './screens/CityList';
import store from "./redux/store";
import { Provider } from 'react-redux';



export default function App() {
  return (
    <Provider store={store}>
      <SafeAreaProvider style={styles.container}>
        <TabNavigation/>
      </SafeAreaProvider>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: "100%",
    backgroundColor: '#fff',
    //alignItems: 'center',
    //justifyContent: 'center',
  },
});
