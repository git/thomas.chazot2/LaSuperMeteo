import AsyncStorage from '@react-native-async-storage/async-storage';
import { City } from '../data/stub';
import {FETCH_FAVORITE_CITY} from '../redux/constants';

export const getFavoriteCityStorage = async () => {
  try {
    const cityJson = await AsyncStorage.getItem('favorite_city')
    return cityJson != null ? JSON.parse(cityJson) : null;
  } catch(e) {
    console.log("An error occurred", e);
  }
}