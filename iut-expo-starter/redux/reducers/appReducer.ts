import { City } from "../../data/stub";
import { ADD_FAVORITE_CITY, FETCH_FAVORITE_CITY, FETCH_WEATHER, FETCH_WEATHER_LIST, FETCH_WEATHER_SEARCHED, GET_FAVORITE_CITY, GET_WEATHER, GET_WEATHER_LIST } from "../constants";

const initialState = {
    weatherList : [],
    weatherListSearched : [],
    favoriteCity: null,
    favoriteWeather: null,
    conditionCodes: null
}

const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_FAVORITE_CITY:
            return {...state, favoriteCity: state.favoriteCity};
        case FETCH_WEATHER_LIST:
            return {...state, weatherList: action.payload};
        case FETCH_FAVORITE_CITY:
            return {...state, favoriteCity: action.payload}
        case GET_FAVORITE_CITY:
            return {...state, favoriteCity: state.favoriteCity}
        case GET_WEATHER_LIST:
            return {...state, weatherList: state.weatherList}
        case FETCH_WEATHER_SEARCHED:
            return {...state, weatherListSearched: action.payload}
        case GET_WEATHER:
            return {...state, weatherList: state.favoriteWeather}
        case FETCH_WEATHER:
            return {...state, favoriteWeather: action.payload}
        default:
            return state;
    }
}

export default appReducer;