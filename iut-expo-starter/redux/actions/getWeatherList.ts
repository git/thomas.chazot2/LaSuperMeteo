import { City, Weather } from '../../data/stub';
import {GET_CITIES} from '../constants';
import { setWeatherList } from './setWeatherList';
import { setWeatherSearched } from './setWeatherSearched';

export const getWeatherList = () => {
  return async dispatch => {

    try {
      const cityPromise = await fetch('https://iut-weather-api.azurewebsites.net/cities');

      const cityListJson = await cityPromise.json();

      const cityList: City[] = cityListJson.map((elt: { [x: string]: any; }) => new City(elt["name"], elt["latitude"], elt["longitude"]));

      const promises = cityList.map( (city) =>{
        return fetch('https://iut-weather-api.azurewebsites.net/weather/city/name/'+city.name);
      })
      Promise.all(promises).then((values) => {
        const prom = values.map(async resp => {
          const weatherJson = await resp.json();
          return new Weather(weatherJson["at"], weatherJson["visibility"], weatherJson["weatherType"], weatherJson["weatherDescription"], weatherJson["temperature"], weatherJson["temperatureFeelsLike"], weatherJson["humidity"], weatherJson["windSpeed"], weatherJson["pressure"], new City(weatherJson["city"]["name"], weatherJson["city"]["latitude"], weatherJson["city"]["longitude"]));
        });
        Promise.all(prom).then((values) => {
          dispatch(setWeatherList(values));
          dispatch(setWeatherSearched(values))
        })

      })
    } catch (error) {
      console.log('Error---------', error);
      //You can dispatch to another action if you want to display an error message in the application
      //dispatch(fetchDataRejected(error))
    }
  }
}