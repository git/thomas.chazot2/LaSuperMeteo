import { Weather } from "../../data/stub";
import { FETCH_WEATHER_LIST } from "../constants";

export const setWeatherList = (weather: Weather[]) => {
    return {
      type: FETCH_WEATHER_LIST,
      payload: weather,
    };
  }