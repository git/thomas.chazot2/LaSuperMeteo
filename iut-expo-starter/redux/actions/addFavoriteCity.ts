import { deleteFavoriteCity } from "../../asyncStorange/deleteFavoriteCityStorage";
import { storeFavoriteCity } from "../../asyncStorange/storeFavoriteCity";
import { City } from "../../data/stub";
import { getWeather } from "./getWeather";
import { setFavoriteCity } from "./setFavoriteCity";
import { setWeather } from "./setWeather";

export const addFavoriteCity = (city : City | null) => {
    return async dispatch => {
        try {
            await deleteFavoriteCity()
            await storeFavoriteCity(city)
            dispatch(setFavoriteCity(city))
            if (city != null){
                dispatch(getWeather(city))
            }
            else{
                dispatch(setWeather(null))
            }
        } catch (error) {
            console.log('Error---------', error);
            //You can dispatch to another action if you want to display an error message in the application
            //dispatch(fetchDataRejected(error))
        }
    }
  }