import { Weather } from "../../data/stub";
import { FETCH_WEATHER, FETCH_WEATHER_SEARCHED } from "../constants";

export const setWeatherSearched = (weather: Weather[]) => {
    return {
      type: FETCH_WEATHER_SEARCHED,
      payload: weather,
    };
  }