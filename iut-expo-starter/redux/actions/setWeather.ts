import { Weather } from "../../data/stub";
import { FETCH_WEATHER } from "../constants";

export const setWeather = (weather: Weather | null) => {
    return {
      type: FETCH_WEATHER,
      payload: weather,
    };
  }