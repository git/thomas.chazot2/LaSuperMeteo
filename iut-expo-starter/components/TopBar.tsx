import React from "react"
import { View, Text, StyleSheet, Platform } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";

export function TopBar(){
const insets = useSafeAreaInsets();
const statusBarHeight = insets.top;
    return (
        <View style={{width : '100%',
        backgroundColor : '#E9967A',
        alignItems : 'center',
        flexDirection : 'row',
        justifyContent : 'space-evenly',
        marginTop : statusBarHeight}}>
            <Text style={TopBarStyle.title}>La Super Météo</Text>
        </View>
    );
}

const TopBarStyle = StyleSheet.create({
    title : {
        fontSize : 30,
        fontWeigh : 'bold',
        color : '#B6D0E2'
    }
});
