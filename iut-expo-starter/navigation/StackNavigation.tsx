import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import React from "react";
import { SafeAreaProvider } from "react-native-safe-area-context";
import CityDetails from "../screens/CityDetails";
import CityList from "../screens/CityList";

export default function StackNavigation() {
    
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator initialRouteName="CityList">
          <Stack.Screen name="CityList" component={CityList} options={{
                                headerShown: false,
                            }}/>
          <Stack.Screen name="CityDetails" component={CityDetails} options={{
                                headerShown: false,
                            }}/>
        </Stack.Navigator>


    )
  }