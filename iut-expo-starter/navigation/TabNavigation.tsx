import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import CityList from "../screens/CityList";
import Home from "../screens/Home";
import StackNavigation from "./StackNavigation";
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';

export default function TabNavigation() {

    const BottomTabNavigator = createBottomTabNavigator();
    return (
        <NavigationContainer>
            <BottomTabNavigator.Navigator initialRouteName="Favorite City" >
                <BottomTabNavigator.Screen name="Favorite City" component={Home}
                            options={{
                                title: 'Favorite City',
                                headerShown: false,
                                tabBarIcon: ({color}) => <AntDesign name="home" size={24} color="black" />
                            }}/>
                <BottomTabNavigator.Screen name="City List" component={StackNavigation}
                            options={{
                                headerShown: false,
                                title: 'City List',
                                tabBarIcon: ({color}) => <Ionicons name="menu-sharp" size={24} color="black" />
                                
                            }}/>
            </BottomTabNavigator.Navigator>
        </NavigationContainer>
    )
}
