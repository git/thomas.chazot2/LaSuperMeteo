import { View, Text, StyleSheet, TouchableHighlight, Image } from "react-native"
import { FlatList, TextInput } from "react-native-gesture-handler"
import { CITIES_DATA, City, getCurrentWeather, FAVORITE_CITY_DATA, Weather } from "../data/stub"
import { VilleCompopo } from "../components/VilleCompopo";
import { TopBar } from "../components/TopBar";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getWeatherList } from "../redux/actions/getWeatherList";
import { SearchBar } from '@rneui/themed';
import { setWeatherSearched } from "../redux/actions/setWeatherSearched";


export default function CityList({navigation}){

    const weatherList: Weather[] = useSelector(state => state.appReducer.weatherList);
    const weatherListSearched: Weather[] = useSelector(state => state.appReducer.weatherListSearched);


    const favoriteCity = useSelector(state => state.appReducer.favoriteCity)

    // Create a const that will hold the react-redux events dispatcher
    const dispatch = useDispatch();
    
    const [cityName, setCityName] = useState('');


    
    // Let's define a hook that will be used to update the rendered state after the return will be called
    // You cannot perform side-effects outside of a useEffect hook
    useEffect(() => {
      const loadWeather = async () => {
        dispatch(getWeatherList());
      };
      loadWeather();
    }, [dispatch]);
    

    function updateSearch(newVal: string){
        if (newVal == ""){
            dispatch(setWeatherSearched(weatherList))
        }
        else{
            dispatch(setWeatherSearched(weatherList.filter((arg) => arg.city.name.toLowerCase().startsWith(newVal.toLowerCase()))))
        }
    }

    return (
        <View style={{flex: 1, width: "100%", alignItems: "center"}}>
            <TopBar/>
            <SearchBar
            platform="default"
            containerStyle={{width: "100%", backgroundColor: "darksalmon"}}
            inputContainerStyle={{backgroundColor: "powderblue"}}
            inputStyle={{}}
            leftIconContainerStyle={{}}
            rightIconContainerStyle={{}}
            loadingProps={{}}
            onChangeText={newVal => {setCityName(newVal); updateSearch(newVal)}}
            placeholder="Type query here..."
            placeholderTextColor="#888"
            round
            value={cityName}
            /> 

            <FlatList
            data={weatherListSearched}
            keyExtractor={item =>item.city.name}
            renderItem={({item}) => <TouchableHighlight onPress={() => navigation.navigate("CityDetails", {"weather": item})}><VilleCompopo weather={item} fav={favoriteCity}/></TouchableHighlight>}
            style={leStyle.container} 
            />
        </View>
    )
}

const leStyle = StyleSheet.create({
    container: {
        flex: 1,
        alignContent: 'center',
        width: '100%'
    },
    searchBar: {
        width: "90%",
        margin: 20,
        height: 30,
        backgroundColor: "powderblue",
        borderRadius: 10,
        textAlign: "center"
    }
});