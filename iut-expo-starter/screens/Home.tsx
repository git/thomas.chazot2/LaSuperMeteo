import { View, Text, StyleSheet, ImageBackground } from "react-native"
import { FlatList } from "react-native-gesture-handler"
import { City, Weather } from "../data/stub"
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { VilleCompopo } from "../components/VilleCompopo";
import { TopBar } from "../components/TopBar";
import CityDetails from "./CityDetails";
import { useDispatch, useSelector } from "react-redux";
import React, { useEffect } from "react";
import { getFavoriteCity } from "../redux/actions/getFavoriteCity";
import { Details } from "../components/Details";
import { getWeather } from "../redux/actions/getWeather";


export default function Home(navigation){

    
    const insets = useSafeAreaInsets();
    const statusBarHeight = insets.top;

    const favoriteCity: City | null = useSelector(state => state.appReducer.favoriteCity);
    const weather: Weather | null = useSelector(state => state.appReducer.favoriteWeather);


    const dispatch = useDispatch();
    
    useEffect(() => {
      const loadFavoriteCity = async () => {
        await dispatch(getFavoriteCity());
      };
      loadFavoriteCity();
    }, [dispatch]);

    return (
        <View style={{alignItems: "center", width: "100%"}}>
            <TopBar/>
            {(() => {
              if (weather != null){
                return (
                    <Details weather={weather}/>
                )
              }
              else{
                return(
                    <Text style={leStyle.text}>Vous n'avez pas ajouté de ville en favoris</Text>
                  )
              }
            })()}
        </View>
    )
}


const leStyle = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    text: {
      fontWeight: "normal",
      fontStyle: 'italic',
      fontSize: 10,
      alignSelf: "center",
      alignContent: "center",
      marginLeft: 7
  },
});