import { ExclusiveGesture } from 'react-native-gesture-handler/lib/typescript/handlers/gestures/gestureComposition';
import { City, FAVORITE_CITY_DATA, WEATHER_DATA } from '../data/stub';
import { getFavoriteCity } from '../redux/actions/getFavoriteCity';
import { setFavoriteCity } from '../redux/actions/setFavoriteCity';
import { setWeather } from '../redux/actions/setWeather';

describe("Actions tests", () => {
    it('should create an action with FETCH_FAVORITE_CITY type', () => {
        const expectation = {
            type : 'FETCH_FAVORITE_CITY',
            payload : FAVORITE_CITY_DATA
        }
        expect(setFavoriteCity(FAVORITE_CITY_DATA)).toEqual(expectation);
    })
    it('should create an action with FETCH_WEATHER type', () => {
        const expectation = {
            type : 'FETCH_WEATHER',
            payload : WEATHER_DATA
        }
        expect(setWeather(WEATHER_DATA)).toEqual(expectation);
    })
})