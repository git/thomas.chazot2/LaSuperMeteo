import React from 'react';
import {expect} from '@jest/globals';
import NounoursListItem from "../components/NounoursListItem";
import {Provider} from "react-redux";
import {configureStore} from "@reduxjs/toolkit";
import {fireEvent, render, screen} from '@testing-library/react-native'
import '@testing-library/jest-native/extend-expect';
import testReducer from "./testReducer";


// To wait for things like animation to be fully loaded.
jest.useFakeTimers();

// Configure store for testing purpose
const store = configureStore({
    reducer: {
        appReducer: testReducer,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false
        })
});

// When using a Data Provider (like redux) in your App, you will need to wrap all your tested component into a test Provider
// You cannot use the exact same provider and store you create in App.tsx file because here you want mocked data into your store
const Wrapper = ({children}) => (<Provider store={store}>{children}</Provider>);

describe('<NounoursListItem />', () => {
    test('Assert displayed values', () => {
        const expectedNounoursInfos = store.getState().appReducer.nounours[0];

        render(<Wrapper>
            <NounoursListItem item={expectedNounoursInfos}/>
        </Wrapper>)

        // Get displayed text component and assert that its value contains our nounours name
        expect(screen.getByTestId('nounours-name')).toHaveTextContent(expectedNounoursInfos.name)
        expect(screen.getByTestId('nounours-nbPoils')).toHaveTextContent(expectedNounoursInfos.nbPoils)
        expect(screen.getByTestId('nounours-age')).toHaveTextContent(expectedNounoursInfos.age)

        expect(screen.getByTestId("nounours-image")).toHaveProp("source", {uri: expectedNounoursInfos.image})

        let nounoursListSize = store.getState().appReducer.nounours.length;
        // Use @testing-library to press our "delete" button 
        fireEvent.press(screen.getByTestId("remove-button"))
        // Make sure that this press action has delete our nounours from our current state (in our test store).
        expect(store.getState().appReducer.nounours.length).toBe(nounoursListSize - 1)
    })
});