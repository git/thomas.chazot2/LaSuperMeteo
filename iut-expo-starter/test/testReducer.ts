import {WEATHER_DATA} from '../data/stub';
import appReducer from "../redux/reducers/appReducer";

// Mock values used in tests
const initialState = {
  weatherList: WEATHER_DATA, //Here you defined a static list loaded in the initial state. In your real store, this list is usually empty.
  favoriteCity: [],
}

// Reducer for tests => Just call the "true" reducer with our mocked data as initial state
// @ts-ignore
export default testReducer = (state = initialState, action) => {
  return appReducer(initialState, action);
}