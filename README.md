# LaSuperMeteo 

![React Native](https://img.shields.io/badge/reactnative-3DDC84?style=for-the-badge&logo=react&logoColor=white)
![TypeScript](https://img.shields.io/badge/typescript-%237F52FF.svg?style=for-the-badge&logo=typescript&logoColor=white) 

La Super Météo est une application qui vous permet de connaître la météo des grandes villes de France. Elle utilise l'API : https://iut-weather-api.azurewebsites.net/swagger-ui/#/Cities/get_cities

Cette application permet aussi d'ajouter une ville en favorite pour accéder à sa météo plus facilement.

## Description

Sur la page d'accueil de l'application, est affichée la ville favorite de l'utilisateur avec toutes ses données métrologiques. Si l'utilisateur n'a pas choisi de ville favorite alors il lui est conseillé d'en mettre une.

Ensuite, il y a une autre page contenant la liste des villes de l'API. L'utilisateur peut aussi rechercher une ville en utilisant la barre de recherche. Si l'utilisateur clique sur un des composants de la ville alors les détails de cette ville s'afficheront.

## Sketchs


<div align = center>
<img alt="home_page_sketch" src="documentation/HomeScreen.png" width="200" >
<img alt="city_list_sketch" src="documentation/ListScreen.png" width="200" >
</div>


## ScreenShots

<div align = center>
<img alt="home_page" src="documentation/HomeScreenSM.png" width="200" >
<img alt="city_list" src="documentation/ListScreenSM.png" width="200" >
<img alt="search_screen" src="documentation/SearchScreenSM.png" width="200" >
</div>


## Notation

Nous avons fait tout ce qui était requis dans la partie basics. Pour la partie application features, nous avons tout à l'exception des tests unitaires qui ne sont pas encore complétement finis.